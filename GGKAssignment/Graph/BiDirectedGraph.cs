﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GGKAssignment.Graph
{
    internal partial class BiDirectedGraph
    {
        private readonly Dictionary<string, Node> _nodes = new Dictionary<string, Node>();

        public void InsertNode(string node)
        {
            if (_nodes.ContainsKey(node))
            {
                Console.WriteLine("Node with name: " + node + " already exists!");
            }
            else
                _nodes.Add(node, new Node(node));
        }

        public void InsertEdge(string node1, int weight, string node2)
        {

            if (!_nodes.ContainsKey(node1))
            {
                Console.WriteLine("Node with name: " + node1 + " does not exist! Please create the node and try again.");
                return;
            }
            if (!_nodes.ContainsKey(node2))
            {
                Console.WriteLine("Node with name: " + node2 + " does not exist! Please create the node and try again.");
                return;
            }
            if (weight < 0)
            {
                Console.WriteLine("Weight of an edge cannot be negative!");
                return;
            }

            var edge = new Edge(weight, _nodes[node2]);
            _nodes[node1].Children.Add(edge);

            edge = new Edge(weight, _nodes[node1]);
            _nodes[node2].Children.Add(edge);

        }

        public void PrintShortestPathDynamic(string source, string destination)
        {
            /*  
             *  0. Check if the nodes exist in the graph.
             *  1. Create distance map
             *  1.1. Cdd all the nodes into distance map with infinity
             *  2. Create distanceFromSource map
             *  3. Create path map
             *  4. current = distance.extractMin()  { for (current = source; current != null; current = getMin(distance) }
             *  4.1. Add (current, distance) to distanceFromSource map
             *  4.2. Remove current from distance map
             *  5. neighbour = get neighbour of current { for (neighbour in current.children) }
             *  6. if neighbour not in distance map, continue
             *  8. if distance in distance map < distanceFromSource(current) + neighbour.edge(current).weight go to 5.
             *  9. distance(neighbour) = distanceFromSource(current) + neighbour.edge(current).weight
             *  10. path(neighbour) = current
             *  11. loop@5 & loop@4 terminated
             *  12. Traverse path map to get the shortest path from source to destination.
             */

            // 0. check if the nodes exist in the graph.
            if (!_nodes.ContainsKey(source))
            {
                Console.WriteLine("The source node: " + source + " does not exist in the graph!");
                return;
            }
            if (!_nodes.ContainsKey(destination))
            {
                Console.WriteLine("The destination node: " + destination + " does not exist in the graph!");
                return;
            }

            // 1. Create distance map
            var distance = new Dictionary<Node, int>();

            // 1.1. Add all the nodes into distance map with infinity
            foreach (var node in _nodes)
                distance.Add(node.Value, int.MaxValue);

            // 2. Create distanceFromSource map
            var distanceFromSource = new Dictionary<Node, int>();

            // 3. Create path map
            var path = new Dictionary<Node, Node>();

            // 4. current = distance.extractMin()  { for (current = source; current != null; current = getMin(distance) }
            for (Node current = _nodes[source]; current != null; current = GetMin(distance))
            {
                // 4.1. Add (current, distance) to distanceFromSource map
                distanceFromSource.Add(current, distance[current]);

                // 4.2. Remove current from distance map
                distance.Remove(current);

                // 5. neighbour = get neighbour of current { for (neighbour in current.children) }
                foreach (Edge neighbour in current.Children)
                {
                    // 6. if neighbour not in distance map, continue
                    if (!distance.ContainsKey(neighbour.AdjacentNode)) continue;

                    // 8. if distance in distance map < distanceFromSource(current) + neighbour.edge(current).weight go to 5.
                    int tempDistance = distanceFromSource[current] + neighbour.Weight;
                    if (distance[neighbour.AdjacentNode] < tempDistance) continue;

                    // 9. distance(neighbour) = distanceFromSource(current) + neighbour.edge(current).weight
                    distance[neighbour.AdjacentNode] = tempDistance;

                    // 10. path(neighbour) = current
                    path[neighbour.AdjacentNode] = current;
                }
            }

            // 12. Traverse path map to get the shortest path from source to destination.
            PrintShortestPath(source, destination, path);


        }

        private void PrintShortestPath(string source, string destination, Dictionary<Node, Node> path)
        {
            var shortestPath = new ArrayList();
            for (Node node = _nodes[destination]; node != _nodes[source]; node = path[node])
                shortestPath.Add(node);
            shortestPath.Add(_nodes[source]);

            shortestPath.Reverse();

            Console.WriteLine("The shortest path from node " + source + " to the node " + destination + " is : ");
            foreach (Node node in shortestPath)
                Console.Write(" -> " + node.Name);
            Console.WriteLine();
        }

        private static Node GetMin(Dictionary<Node, int> distance)
        {
            var min = int.MaxValue;
            Node minNode = null;
            foreach (var pair in distance)
                if (pair.Value < min)
                {
                    min = pair.Value;
                    minNode = pair.Key;
                }
            return minNode;
        }

        public bool PrintShortestPathGreedy(string node1, string node2, HashSet<string> visited = null)
        {
            if (visited == null) visited = new HashSet<string>();
            if (visited.Contains(node1)) return false;

            visited.Add(node1);

            if (node1.Equals(node2)) return true;

            foreach (Edge edge in _nodes[node1].Children)
            {
                if (!PrintShortestPathGreedy(edge.AdjacentNode.Name, node2, visited)) continue;

                Console.WriteLine(" -> " + edge.AdjacentNode.Name);
                return true;
            }

            return false;
        }

    }

    internal class Node
    {
        public string Name { get; }
        public SortedSet<Edge> Children { get; }

        public Node(string name)
        {
            Name = name;
            Children = new SortedSet<Edge>();
        }
    }


    internal class Edge : IComparable
    {
        public int Weight { get; }
        public Node AdjacentNode { get; }

        public Edge(int weight, Node adjacentNode)
        {
            Weight = weight;
            AdjacentNode = adjacentNode;
        }


        public int CompareTo(object obj)
        {
            return Weight - ((Edge)obj).Weight;
        }
    }
}
