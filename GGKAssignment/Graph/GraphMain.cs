﻿using System;

namespace GGKAssignment.Graph
{
    internal partial class BiDirectedGraph
    {
        public static void ExecuteMain()
        {
            Console.WriteLine("\n2. Shortest Path");

            var graph = new BiDirectedGraph();

            Console.WriteLine("Enter number of nodes in the graph: ");
            var numNodes = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException("Invalid input"));

            for (var i = 0; i < numNodes; i++)
            {
                Console.WriteLine("Enter the name of Node " + (i + 1) + ": ");
                graph.InsertNode(Console.ReadLine());
            }

            Console.WriteLine("Enter number of edges in the graph: ");
            var numEdges = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException("Invalid input"));

            for (var i = 0; i < numEdges; i++)
            {
                Console.WriteLine("Enter the weight of the edge " + (i + 1) + ": ");
                var weight = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException("Invalid input"));

                Console.WriteLine("Enter the name of first node in the edge" + (i + 1) + ": ");
                string node1 = Console.ReadLine();

                Console.WriteLine("Enter the name of second node in the edge" + (i + 1) + ": ");
                string node2 = Console.ReadLine();

                graph.InsertEdge(node1, weight, node2);
            }

            Console.WriteLine("Enter the source node: ");
            string source = Console.ReadLine();

            Console.WriteLine("Enter the destination node: ");
            string destination = Console.ReadLine();

            graph.PrintShortestPathDynamic(source, destination);
        }
    }
}
