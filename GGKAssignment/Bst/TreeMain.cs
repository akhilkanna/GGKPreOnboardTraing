﻿using System;

namespace GGKAssignment.Bst
{
    internal partial class Tree
    {
        public static void ExecuteMain()
        {
            Console.WriteLine("\n1. Binary Search Tree");
            Console.WriteLine("Enter number of nodes in the tree: ");

            var tree = new Tree();

            var numNodes = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException("Invalid input"));

            for (var i = 0; i < numNodes; i++)
            {
                Console.WriteLine("Enter the value at node " + (i + 1) + ": ");
                var node = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException("Invalid input"));
                tree.Insert(node);
            }

            Console.WriteLine("Binary Search Tree in ascending order: ");
            tree.Print(inAscending: true);

            Console.WriteLine("Binary Search Tree in descebding order: ");
            tree.Print(inAscending: false);

            Console.WriteLine("Enter a node to search: ");
            var searchNode = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException("Invalid input"));

            tree.Search(searchNode);
        }
    }
}
