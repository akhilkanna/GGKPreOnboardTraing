﻿using System;

namespace GGKAssignment.Bst
{
    internal partial class Tree
    {
        private Node _root;

        public void Insert(int data)
        {
            if (_root == null)
                _root = new Node(data);
            else
                Insert(_root, data);
        }

        public void Print(bool inAscending)
        {
            if (_root == null)
                Console.Write("Tree is empty.");
            else
                Print(_root, inAscending);
            Console.WriteLine();
        }

        public bool Search(int element)
        {
            Node node = _root;
            while (node != null)
            {
                if (node.Data > element)
                    node = node.Left;

                else if (node.Data < element)
                    node = node.Right;

                else
                {
                    Console.WriteLine("The element: " + element + " exists in the tree!");
                    return true;
                }
            }
            Console.WriteLine("The element: " + element + " is not found in the tree!");
            return false;
        }

        //--------PRIVATE METHODS---------------

        private static void Insert(Node node, int data)
        {
            if (node.Data == data)
                Console.WriteLine("Node: " + data + " already exists!");
            else if (node.Left == null && node.Data > data)
                node.Left = new Node(data);
            else if (node.Right == null && node.Data < data)
                node.Right = new Node(data);
            else if (node.Data > data)
                Insert(node.Left, data);
            else if (node.Data < data)
                Insert(node.Right, data);
            else
                Console.Write("Duplicate data node found : " + data);
        }

        private static void Print(Node node, bool inAsceding)
        {
            if (node == null) return;

            Print(inAsceding ? node.Left : node.Right, inAsceding);

            Console.Write(node.Data + " ");

            Print(inAsceding ? node.Right : node.Left, inAsceding);
        }

    }

    internal class Node
    {
        public int Data { get; set; }
        public Node Left, Right;

        public Node(int data)
        {
            Data = data;
            Left = null;
            Right = null;
        }

    }
}
