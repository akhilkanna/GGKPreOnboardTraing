﻿using System;
using System.Text;

namespace GGKAssignment.Addition
{
    internal partial class Sum
    {
        private readonly string _number1;
        private readonly string _number2;

        public Sum(string num1, string num2)
        {
            _number1 = num1;
            _number2 = num2;

            if (_number1.Length >= _number2.Length) return;

            string temp = _number1;
            _number1 = _number2;
            _number2 = temp;
        }

        public string Execute()
        {
            var sum = new StringBuilder();

            var carry = 0;

            //calculate sum until the length of smaller number
            for (var i = 0; i < _number2.Length; i++)
            {
                if (!IsNumber(_number2[_number2.Length - 1 - i]) || !IsNumber(_number1[_number1.Length - 1 - i]))
                {
                    Console.Write("The number must be valid.");
                    return null;
                }

                int result = carry + _number2[_number2.Length - 1 - i] + _number1[_number1.Length - 1 - i] - 2 * '0';
                if (result > 9)
                {
                    carry = 1;
                    result -= 10;
                }
                sum.Append(result.ToString());
            }

            //append the reamaining numbers in larger number to the result
            for (int i = _number1.Length - _number2.Length - 1; i >= 0; i--)
            {
                if (!IsNumber(_number1[i]))
                {
                    Console.Write("The number must be valid.");
                    return null;
                }

                if (carry == 1)
                {
                    sum.Append(carry + _number1[i] - '0');
                    carry = 0;
                }
                else
                    sum.Append(_number1[i] - '0');
            }

            return Reverse(sum.ToString());
        }

        private static bool IsNumber(char ch)
        {
            return ch >= '0' && ch <= '9';
        }

        private static string Reverse(string str)
        {
            char[] charArray = str.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
