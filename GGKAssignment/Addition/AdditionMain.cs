﻿using System;

namespace GGKAssignment.Addition
{
    internal partial class Sum
    {
        public static void ExecuteMain()
        {
            Console.WriteLine("\n4. Adding two large integers");

            Console.WriteLine("Enter first number to add: ");
            string number1 = Console.ReadLine();

            Console.WriteLine("Enter second number to add: ");
            string number2 = Console.ReadLine();

            Console.WriteLine(new Sum(number1, number2).Execute());
        }
    }
}
