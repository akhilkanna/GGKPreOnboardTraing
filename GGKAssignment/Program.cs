﻿using System;
using GGKAssignment.Bst;
using GGKAssignment.Primes;
using GGKAssignment.Addition;
using GGKAssignment.Ascii;
using GGKAssignment.Graph;

namespace GGKAssignment
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("GGK ASSIGNMENT 1 BY AKHIL KANNA D");

            while (true)
            {
                try
                {
                    Console.WriteLine(
                        "\nChoose a program to run: \n1. BST\n2. Graph - shortest path\n3. Consecutive Prime Sum\n4. Adding two numbers\n5. ASCII Conversion\n6. Exit\n");

                    string choice = Console.ReadLine();

                    switch (choice)
                    {
                        case "1":
                            Tree.ExecuteMain();
                            break;
                        case "2":
                            BiDirectedGraph.ExecuteMain();
                            break;
                        case "3":
                            ConsecutivePrimeSum.ExecuteMain();
                            break;
                        case "4":
                            Sum.ExecuteMain();
                            break;
                        case "5":
                            Conversion.ExecuteMain();
                            break;
                        case "6":
                            return;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
