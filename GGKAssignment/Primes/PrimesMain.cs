﻿using System;

namespace GGKAssignment.Primes
{
    internal partial class ConsecutivePrimeSum
    {
        public static void ExecuteMain()
        {
            Console.WriteLine("\n3. Consecutive Prime Sum");
            Console.WriteLine("Enter a number to find number of such primes: ");

            if (int.TryParse(Console.ReadLine(), out int number))
                new ConsecutivePrimeSum(number).Execute();
            else
            {
                Console.WriteLine("Invalid input, try again.");
                ExecuteMain();
            }
        }
    }
}
