﻿using System;
using System.Collections;

namespace GGKAssignment.Primes
{
    internal partial class ConsecutivePrimeSum
    {
        private readonly int _n;

        public ConsecutivePrimeSum(int n)
        {
            _n = n;
        }

        public void Execute()
        {
            if (_n < 3)
            {
                Console.WriteLine("N should be greater than or equal to 3!");
                return;
            }

            var result = 0;
            var primeConsecutives = new ArrayList { 2 };

            for (var i = 3; i <= _n; i++)
            {
                if (!IsPrime(i, primeConsecutives)) continue;

                primeConsecutives.Add(i);

                if (IsSumFound(i, primeConsecutives)) result++;

            }

            Console.WriteLine("Number of such numbers = " + result);
        }

        private static bool IsSumFound(int number, IEnumerable primeConsecutives)
        {
            var sum = 0;
            foreach (int prime in primeConsecutives)
            {
                sum += prime;
                if (sum == number)
                    return true;
            }
            return false;
        }

        private static bool IsPrime(int number, IEnumerable primeConsecutives)
        {
            foreach (int prime in primeConsecutives)
                if (number % prime == 0)
                    return false;

            return true;
        }
    }
}
