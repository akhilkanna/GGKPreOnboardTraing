﻿using System;
using System.Text;

namespace GGKAssignment.Ascii
{
    internal class Conversion
    {
        private readonly string _word;

        public Conversion(string word)
        {
            _word = word;
        }

        public static void ExecuteMain()
        {
            Console.WriteLine("\n5. ASCII Conversion");

            //Read string
            Console.WriteLine("Enter a string to make ASCII conversions: ");
            string input = Console.ReadLine();
            Console.WriteLine(new Conversion(input).Convert());
        }

        public string Convert()
        {
            int[] asciiValues = new int[_word.Length - 1];

            var result = new StringBuilder();

            for (var i = 0; i < asciiValues.Length; i++)
            {
                asciiValues[i] = (_word[i] + _word[i + 1]) / 2;
                if (IsPrime(asciiValues[i]))
                    asciiValues[i] += 1;
                result.Append((char)asciiValues[i]);
            }

            return result.ToString();
        }

        private static bool IsPrime(int number)
        {
            for (var i = 2; i < Math.Sqrt(number); i++)
                if (number % i == 0)
                    return false;
            return true;
        }
    }
}
