﻿using System;
using System.Collections.Generic;

namespace Assignment2.Pattern
{
    /// <summary>
    ///     Reads fibonacci length and prints a prime fibonacci pattern
    /// </summary>
    internal class PrimeFibonacci
    {
        private int _number;

        /// <summary>
        ///     Read the input from user and assigns it to _number
        /// </summary>
        public void Execute()
        {
            Console.WriteLine("\n2. Prime Fibonacci Pattern");
            Console.WriteLine("Enter a number to print prime fibonacci pattern: ");

            if (int.TryParse(Console.ReadLine(), out _number))
                PrintPattern(GetPrimeFibonacciNumbers());
            else
            {
                Console.WriteLine("Invalid input, try again.");
                Execute();
            }
        }

        /// <summary>
        ///     Prints the pattern with the numbers in the parameter
        /// </summary>
        /// <param name="primeFibonacciNumbers">An array of all the prime fibonacci numbers needed to print the pattern</param>
        private void PrintPattern(int[] primeFibonacciNumbers)
        {
            for (var i = 0; i < primeFibonacciNumbers.Length; i++)
            {
                for (var j = 0; j <= i; j++)
                {
                    Console.Write(primeFibonacciNumbers[j]+" ");
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        ///     Returns an array of all prime fibonacci numbers till the given limit (_number)
        /// </summary>
        /// <return>Array of all prime fibonacci numbers till _number</return>
        private int[] GetPrimeFibonacciNumbers()
        {
            var primeFibonacciNumbers = new List<int>();

            var number1 = 0;
            var number2 = 1;

            for (var count = 0; count < _number; count++)
            {
                int sum = number1 + number2;

                if (IsPrime(sum))
                {
                    primeFibonacciNumbers.Add(sum);
                }

                number1 = number2;
                number2 = sum;
            }

            return primeFibonacciNumbers.ToArray();
        }

        /// <summary>
        ///     Checks if a number is prime or not.
        /// </summary>
        /// <param name="number">Number to check if it is prime.</param>
        /// <return>True if number is prime, else False</return>
        private bool IsPrime(int number)
        {
            if (number < 2)
            {
                return false;
            }

            double limit = Math.Sqrt(number);

            for (var i = 2; i < limit; i++)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
