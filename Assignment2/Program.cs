﻿using System;
using Assignment2.BinaryOperations;
using Assignment2.Pattern;

namespace Assignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n-------------------------------------------");
            Console.WriteLine("\nGGK ASSIGNMENT 2 BY AKHIL KANNA D");

            try
            {
                Console.WriteLine(
                    "\nChoose a program to run: \n\n1. Binary Float Multiplication\n2. Prime Fibonacci Pattern\n3. Exit\n");

                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        new FloatMultiplication().Execute();
                        break;
                    case "2":
                        new PrimeFibonacci().Execute();
                        break;
                    case "3":
                        return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: "+e.GetType());
                Console.WriteLine(e.Message);
            }

            Main(null);
        }
    }
}
