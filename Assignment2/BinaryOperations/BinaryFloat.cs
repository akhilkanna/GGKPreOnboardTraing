﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Assignment2.BinaryOperations
{
    /// <summary>
    ///     Class that handles multiplication of float numbers in binary representation.
    /// </summary>
    internal class BinaryFloat
    {
        public float FloatValue { get; }
        public string BinaryValue { get; }

        /// <summary>
        ///     Assigns the float number to FloatValue and also converts that float to binary representation and assigns it to BinaryValue
        /// </summary>
        /// <param name="number">Float number to assign and convert</param>
        public BinaryFloat(float number)
        {
            FloatValue = number;
            BinaryValue = GetBinary(number);
        }

        /// <summary>
        ///     Multiplies current float number (this) with the 'other' float number by converting them into binary
        ///     Convert the product from binary format to float format and return the value.
        /// </summary>
        /// <param name="other">The second float value to multiply.</param>
        /// <return>The product of the two floats in float type</return>
        public float Multiply(BinaryFloat other)
        {
            string plainNumber1 = GetPlainBinaryValue(this);
            string plainNumber2 = GetPlainBinaryValue(other);

            var intermediates = GetIntermediates(plainNumber1, plainNumber2);

            string result = BinarySum(intermediates, GetNumFloatingPoints(this, other));

            float floatResult = GetFloat(result);

            return floatResult;
        }

        /// <summary>
        ///     Converts a floating point number into a binary string
        /// </summary>
        /// <param name="number">The float to convert.</param>
        /// <return>A string representation of the float in binary format.</return>
        private string GetBinary(float number)
        {
            string[] decimals = Convert.ToString(number, CultureInfo.InvariantCulture).Split('.');

            string numberBinary = Convert.ToString(Convert.ToInt32(decimals[0]), 2);

            if (decimals.Length == 2)
            {
                numberBinary += "." + GetDecimalBinary(decimals[1]);
            }

            return numberBinary;
        }

        /// <summary>
        ///     Converts the part of the floating point number after the decimal point into binary format.
        /// </summary>
        /// <param name="number">The part of the float after decimal point.</param>
        /// <return>A string representation of the right side of the float in binary format.</return>
        private string GetDecimalBinary(string number)
        {
            var binaryNumber = new StringBuilder();

            for (var i=0; i<5 && !number.Equals("0"); i++)
            {
                float result = float.Parse("0."+number) * 2;
                string[] decimals = Convert.ToString(result, CultureInfo.InvariantCulture).Split('.');

                binaryNumber.Append(decimals[0]);

                number = decimals.Length == 2 ? decimals[1] : "0";

            }

            return binaryNumber.ToString();
        }

        /// <summary>
        ///     Returns the sum of number of digits after the decimal in both the numbers
        /// </summary>
        /// <param name="number1">First float to find number of decimal point digits</param>
        /// <param name="number2">Second float to find number of decimal point digits</param>
        /// <return>Number of digits after the decimal point in both the numbers. (sum)</return>
        private int GetNumFloatingPoints(BinaryFloat number1, BinaryFloat number2)
        {
            return GetNumFloatingPoints(number1.BinaryValue) + GetNumFloatingPoints(number2.BinaryValue);
        }

        /// <summary>
        ///     Returns the number of digits after the decimal
        /// </summary>
        /// <param name="number">Float to find number of decimal point digits</param>
        /// <return>Number of digits after the decimal point.</return>
        private int GetNumFloatingPoints(string number)
        {
            string[] decimals = number.Split('.');

            return decimals.Length == 2 ? decimals[1].Length : 0;
        }

        /// <summary>
        ///     Converts a binary representation of a float into a float
        /// </summary>
        /// <param name="binary">Binary string to convert to float.</param>
        /// <return>Float value of the binary representation.</return>
        private float GetFloat(string binary)
        {
            string[] decimals = binary.Split('.');

            float intPart = Convert.ToInt32(decimals[0], 2);
            var decimalPart = 0f;

            if (decimals.Length == 2)
            {
                decimalPart = ConvertDecimal(decimals[1]);
            }

            return intPart + decimalPart;
        }

        /// <summary>
        ///     Converts right side of decimal point in binary into float
        /// </summary>
        /// <param name="number">Right side of decimal point in binary format</param>
        /// <return>Right side of decimal point in float</return>
        private float ConvertDecimal(string number)
        {
            var result = 0f;

            for (var i = 0; i < number.Length; i++)
            {
                if (number[i] == '1')
                {
                    result += (float)(1 / Math.Pow(2, i + 1));
                }
            }

            return result;
        }

        /// <summary>
        ///     Finds sum of all the binary numbers in 'numbers' list.
        /// </summary>
        /// <param name="numbers">List of all the binary numbers</param>
        /// <param name="numDecimalPoints">Number of digits after the decimal point.</param>
        /// <return>Sum of all the binary float numbers in the given list.</return>
        private string BinarySum(List<string> numbers, int numDecimalPoints)
        {
            string result = numbers[0];

            for (var i = 1; i < numbers.Count; i++)
            {
                result = BinarySum(result, numbers[i]);
            }

            return result.Insert(result.Length-numDecimalPoints, ".");
        }

        /// <summary>
        ///     Finds sum of two binary numbers
        /// </summary>
        /// <param name="number1">First binary number to add.</param>
        /// <param name="number2">Second binary number to add.</param>
        /// <return>Sum of the two binary numbers in binary format</return>
        private string BinarySum(string number1, string number2)
        {
            int intNumber1 = Convert.ToInt32(number1, 2);
            int intNumber2 = Convert.ToInt32(number2, 2);

            return Convert.ToString(intNumber1 + intNumber2, 2);
        }

        /// <summary>
        ///     Gets all the intermediate binary numbers to add in the binary multiplication
        /// </summary>
        /// <param name="number1">First binary number to multiply.</param>
        /// <param name="number2">Second binary number to multiply.</param>
        /// <return>List of all the intermediate binary numbers to add in the binary multiplication</return>
        private List<string> GetIntermediates(string number1, string number2)
        {
            var intermediates = new List<string>();

            for (var i = 0; i < number2.Length; i++)
            {
                if (number2[i] == '0')
                {
                    continue;
                }

                var intermediate = new StringBuilder();
                intermediate.Append(GetZeros(i));

                foreach (char bit in number1)
                {
                    intermediate.Append(MultiplyBits(number2[i], bit));
                }

                intermediates.Add(Reverse(intermediate.ToString()));
            }

            return intermediates;
        }

        /// <summary>
        ///     Removes the decimal point in the binary string and reverses it. 
        ///     This makes traversing the bits easier for multiplication.
        /// </summary>
        /// <param name="number">Binary float number</param>
        /// <return>Reversed binary number without the decimal point.</return>
        private string GetPlainBinaryValue(BinaryFloat number)
        {
            string plain = number.BinaryValue.Replace(@".", string.Empty);
            return Reverse(plain);
        }

        /// <summary>
        ///     Reverses a string
        /// </summary>
        /// <param name="myString">String to be reversed.</param>
        /// <return>Reversed string.</return>
        private string Reverse(string myString)
        {
            char[] charArray = myString.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        /// <summary>
        ///     Returns a string of '0's in length specified.
        /// </summary>
        /// <param name="size">Size of the zeros string</param>
        /// <return>String with '0's of given size.</return>
        private string GetZeros(int size)
        {
            var zeros = new StringBuilder();
            for (var i = 0; i < size; i++)
                zeros.Append("0");
            return zeros.ToString();
        }

        /// <summary>
        ///     Logic for bit multiplication. 
        ///     1 x 1 = 1
        ///     1 x 0 = 0
        ///     0 x 1 = 0
        ///     0 x 0 = 0
        /// </summary>
        /// <param name="bit1">First binary bit to multiply.</param>
        /// <param name="bit2">Second binary bit to multiply.</param>
        /// <return>Result of the bit multiplication.</return>
        private char MultiplyBits(char bit1, char bit2)
        {
            if (bit1 == '0' || bit2 == '0')
                return '0';
            return '1';
        }

    }
}
