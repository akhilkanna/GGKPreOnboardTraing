﻿using System;

namespace Assignment2.BinaryOperations
{
    internal class FloatMultiplication
    {
        private BinaryFloat _number1, _number2;

        /// <summary>
        ///     Reads two float numbers and prints their product
        /// </summary>
        /// <return>A string representation of the double's exact decimal value.</return>
        public void Execute()
        {
            Console.WriteLine("\n1. Binary Float Multiplication");

            Console.WriteLine("Enter a number to multiply: ");

            // Reading first number
            if (float.TryParse(Console.ReadLine(), out var number1))
            {
                _number1 = new BinaryFloat(number1);
                Console.WriteLine("Enter another number to multiply: ");

                // Reading second number
                if (float.TryParse(Console.ReadLine(), out var number2))
                {
                    _number2 = new BinaryFloat(number2);

                    // Printing the product result
                    Console.WriteLine("Product of the given numbers = " + _number1.Multiply(_number2));

                    return;
                }
            }

            Console.WriteLine("Invalid input, try again.");
            Execute();
        }
    }
}