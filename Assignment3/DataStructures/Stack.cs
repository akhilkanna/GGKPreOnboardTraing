﻿using System;
using System.Collections.Generic;

namespace Assignment3.DataStructures
{
    internal class Stack<T>
    {
        private readonly List<T> _list;

        public Stack()
        {
            _list = new List<T>();
        }

        public bool IsEmpty()
        {
            return _list == null || _list.Count == 0;
        }

        public object Pop()
        {
            if (_list.Count == 0)
            {
                Console.WriteLine("Stack is empty");
                return null;
            }
            T item = _list[_list.Count - 1];
            _list.RemoveAt(_list.Count - 1);
            Console.WriteLine("Item popped: " + item);
            return item;
        }

        public object Peek()
        {
            if (_list.Count == 0)
            {
                Console.WriteLine("Stack is empty");
                return null;
            }
            return _list[_list.Count - 1];
        }

        public void Push(T item)
        {
            _list.Add(item);
        }

        public void Display()
        {
            foreach (T item in _list)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

        public static void Main()
        {
            var stack = new Stack<string>();

            while (stack != null)
            {
                Console.WriteLine("\n3. Stack");

                Console.WriteLine("\nEnter your choice: \n1) Push\n2) Pop\n3) Peek\n4) Exit");

                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Console.WriteLine("Enter the string to be pushed: ");
                        stack.Push(Console.ReadLine());
                        break;
                    case "2":
                        stack.Pop();
                        break;
                    case "3":
                        Console.WriteLine(stack.Peek());
                        break;
                    case "4":
                        stack = null;
                        break;
                }
            }
        }
    }
}
