﻿using System;
using System.Collections.Generic;


namespace Assignment3.DataStructures
{
    internal class Queue<T>
    {
        private readonly List<T> _list;

        public Queue()
        {
            _list = new List<T>();
        }

        public bool IsEmpty()
        {
            return _list == null || _list.Count == 0;
        }

        public void Enqueue(T item)
        {
            _list.Add(item);
        }

        public object Dequeue()
        {
            if (_list.Count == 0)
            {
                Console.WriteLine("Queue is empty");
                return null;
            }
            T item = _list[0];
            _list.RemoveAt(0);
            Console.WriteLine("Item popped: " + item);
            return item;
        }

        public object Peek()
        {
            if (_list.Count == 0)
            {
                Console.WriteLine("Queue is empty");
                return null;
            }
            return _list[0];
        }

        public void Display()
        {
            foreach (T item in _list)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

        public static void Main()
        {
            var queue = new Queue<string>();

            while (queue != null)
            {
                Console.WriteLine("\n4. Queue");

                Console.WriteLine("\nEnter your choice: \n1) Enqueue\n2) Dequeue\n3) Peek\n4) Exit");

                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Console.WriteLine("Enter the string to be enqueued: ");
                        queue.Enqueue(Console.ReadLine());
                        break;
                    case "2":
                        queue.Dequeue();
                        break;
                    case "3":
                        Console.WriteLine(queue.Peek());
                        break;
                    case "4":
                        queue = null;
                        break;
                }
            }
        }
    }
}
