﻿using System;

namespace Assignment3.SearchTree
{
    internal class Node<T> where T : IComparable<T>
    {
        public T Element { get; }
        public Node<T> Left { get; set; }
        public Node<T> Right { get; set; }

        public Node(T element)
        {
            Element = element;
            Left = null;
            Right = null;
        }
    }
}
