﻿using System;

namespace Assignment3.SearchTree
{
    internal class Tree<T> where T : IComparable<T>
    {
        public Node<T> Root { get; }

        public Tree(T root)
        {
            Root = new Node<T>(root);
        }


        public void Insert(T element)
        {
            Insert(Root, element);
        }

        private void Insert(Node<T> node, T element)
        {
            if (element.CompareTo(node.Element) < 0)
            {
                if (node.Left == null)
                {
                    node.Left = new Node<T>(element);
                }
                else
                {
                    Insert(node.Left, element);
                }
            }
            else if (element.CompareTo(node.Element) > 0)
            {
                if (node.Right == null)
                {
                    node.Right = new Node<T>(element);
                }
                else
                {
                    Insert(node.Right, element);
                }
            }
            else
            {
                Console.WriteLine("Duplicate element.");
            }
        }

        public void Display()
        {
            Console.WriteLine("Elements in the tree in order: ");
            Display(Root);
        }

        private void Display(Node<T> node)
        {
            if (node == null)
            {
                return;
            }
            Display(node.Left);
            Console.WriteLine("-> "+node.Element);
            Display(node.Right);
        }
    
        public static void Main()
        {
            Tree<Car> tree = null;


            Console.WriteLine("\n2. A. Search Tree of IComparables");
            Console.WriteLine("Enter the number of nodes: ");

            if (int.TryParse(Console.ReadLine(), out int numNodes))
            {
                for (var i = 0; i < numNodes; i++)
                {

                    Console.WriteLine("Enter the name of car " + (i+1) + " :");
                    string name = Console.ReadLine();

                    Console.WriteLine("Enter the mileage of car " + (i+1) + " - "+name+": ");
                    if (double.TryParse(Console.ReadLine(), out double mileage))
                    {
                        var car = new Car(name, mileage);

                        if (tree == null)
                        {
                            tree = new Tree<Car>(car);
                        }
                        else
                        {
                            tree.Insert(car);
                        }

                    }
                    else
                    {
                        Console.WriteLine("Invalid input, try again.");
                        i--;
                    }
                }
                tree?.Display();
                return;
            }

            Console.WriteLine("Invalid input, try again.");
            Main();
            

        }
    }
}
