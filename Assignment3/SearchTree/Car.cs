﻿using System;
using System.Globalization;

namespace Assignment3.SearchTree
{
    internal class Car : IComparable<Car>
    {
        private readonly double _mileage;
        public string Name { get; set; }

        public Car(string name, double mileage)
        {
            Name = name;
            _mileage = mileage;
        }


        public int CompareTo(Car other)
        {
            return (int) Math.Round(_mileage - other._mileage);
        }

        public override string ToString()
        {
            return "Name = "+Name+" | Mileage = "+_mileage.ToString(CultureInfo.InvariantCulture);
        }
    }
}
