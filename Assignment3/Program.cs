﻿using System;
using Assignment3.SearchTree;
using Assignment3.Vehicle;

namespace Assignment3
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            Console.WriteLine("\n-------------------------------------------");
            Console.WriteLine("\nGGK ASSIGNMENT 3 BY AKHIL KANNA D");

            try
            {
                Console.WriteLine(
                    "\nChoose a program to run: \n\n1. Vehicle Factory\n2. Search Tree\n3. Stack \n4. Queue\n5. Exit\n");

                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        new Factory().Main();
                        break;
                    case "2":
                        Tree<SearchTree.Car>.Main();
                        break;
                    case "3":
                        DataStructures.Stack<string>.Main();
                        break;
                    case "4":
                        DataStructures.Queue<string>.Main();
                        break;
                    case "5":
                        return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.GetType());
                Console.WriteLine(e.Message);
            }

            Main(null);

        }
    }
}
