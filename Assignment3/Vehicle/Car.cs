﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3.Vehicle
{
    internal class Car : IVehicle
    {
        public IVehicle Build()
        {
            Console.WriteLine("Building new Car");
            return this;
        }
    }
}
