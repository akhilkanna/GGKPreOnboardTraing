﻿using System;

namespace Assignment3.Vehicle
{
    internal class Factory
    {
        public void Main()
        {
            Console.WriteLine("\n1. Number Factory");
            Console.WriteLine("Enter a type of vehicle to create (Bus, Car or Truck) : ");

            string inputType = Console.ReadLine();

            BuildVehicle(inputType);

        }

        private IVehicle BuildVehicle(string type)
        {
            IVehicle vehicle;

            if (string.Equals(type, "Bus", StringComparison.OrdinalIgnoreCase))
            {
                vehicle = new Bus();
            }
            else if (string.Equals(type, "Car", StringComparison.OrdinalIgnoreCase))
            {
                vehicle = new Car();
            }
            else if (string.Equals(type, "Truck", StringComparison.OrdinalIgnoreCase))
            {
                vehicle = new Truck();
            }
            else
            {
                throw new Exception("Invalid type of vehicle");
            }

            return vehicle.Build();
        }
    }
}
