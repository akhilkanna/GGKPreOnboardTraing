﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testing.BST;


namespace Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            var tree = new Tree();

            tree.Insert(7);
            tree.Insert(3);
            tree.Insert(1);
            tree.Insert(5);
            tree.Insert(4);
            tree.Insert(6);
            tree.Insert(11);
            tree.Insert(9);
            tree.Insert(8);
            tree.Insert(13);
            tree.Insert(12);
            tree.Insert(14);

            tree.Print(inAscending:true);
            tree.Print(inAscending:false);

            tree.Search(6);

        }
    }
}
